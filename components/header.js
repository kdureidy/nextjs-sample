function MyHeader({ Component, pageProps }) {
    return <>
        <header className="header-fixed">

            <div className="header-limiter">

                <h1><a href="#">KD<span>Blog</span></a></h1>

                <nav>
                    <a href="#">Home</a>
                    <a href="#" className="selected">Blog</a>
                    <a href="#">Pricing</a>
                    <a href="#">About</a>
                    <a href="#">Faq</a>
                    <a href="#">Contact</a>
                </nav>

            </div>

        </header>
    </>
}

export default MyHeader